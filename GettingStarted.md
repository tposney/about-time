### Getting Started
You must have simple calendar installed to use about time

###
The library allows you to trigger events in the world.


```
game.Gametime.reminderIn({minute: 30}, "***Here is a reminder***"); // send a message to the console in 30 minutes - not really all that useful
```

```
game.Gametime.doIn({minute: 30}, "Time Passes"); // run a macro in 30 minutes - to have this work create a chat macro called "Time Passes"
```

```
game.Gametime.doEvery({minute: 30}, "Time Passes") // run the macro every 30 minutes.
```

```
game.Gametime.doAt({hour: 19, minute: 0}, "Time Passes")
```

```
value = 10
game.Gametime.notifyEvery({minute: 30}, "My event", "this", "and", value)
```
When the event occurs ```Hooks.call("eventTrigger", "My event", this, "and", 10)``` will be called on every client.
So as a programmer you can do a Hooks.on("eventTrigger", handler) to cause something to happen on every client.

All of the triggers do, reminder, notify come in three falvors, xxxIn, xxxEvery and xxxAt. The xxxIn and xxxEvery take a simpe time parameter, like doIn({minute: 10})...). the xxxAt version require a DateTime of the form {year: X, month: X, day: X, hour: X, minute: X, seconds: X}.

If you set an event/macro/notifcation to happen at a time and the clock is advanced past that time the trigger will fire. So if you have a macro to fire at 7pm and you advance the clock from 6pm to (say) 10pm wihtout stepping through the intermeiate times the event will trigger at 10pm game time.

If you have an event set to run every hour and advance the clock 3 hours the event will trigger 3 times.

### Doing stuff.....
And now the reason I started this whole thing which was to allow conditions/items to time out. 

If you an AcviteEffect on an actor called Bless and want to delete it in one minute.
You will need to define a function
```
function deleteEffect(uuid){const ef = fromUuidSync(uuid); ef?.delete()}
game.Gametime.doIn({minute: 1}, deleteEffect, actor.effects.getName("Bless").uuid)
```

If you create a macro  "Show Time"
```
let dt = SimpleCalendar.api.formatDateTime(SimpleCalendar.api.currentDateTime())
ChatMessage.create({content: `${dt.date} ${dt.time}`})

```

and the execute
```
game.Gametime.doEvery({minute: 15}, "Show Time")
```
then every 15 minutes the game time will be pushed to the chat log. This will condinute indefinitely and persist across restarts.

If this should get annoying or you want to clear an event you can remember the number returned by the doEvery call or use
```
game.Gametime.queue()
```
```about-time |  queue [0] 1581598672547 true {minute: 15} Show Game Time []```
Which shows what event are in the queue. In this case we see that event id 1581598672547 does "Same Time and it is recurring every 15 minutes.
```
game.Gametime.clearTimeout(1581598672547)
``` 
will remove the event from the queue.  

If for whatever reason the event queue gets corrupted it can be cleared out with
```
game.Gametime.flushQueue()
```

There are a few restrictions on handlers because they need to saved to storage which menas they can't be native code and any parametrs need to be pure data, e.g. actorData rather than actors. Better still is to pass the id/uuid of the affected target. So in this case passing the token.id or token.document.uuid means that the handler can get back the correct data. 

**Events** are persistent so when you relaod the game your event queue will load. 

